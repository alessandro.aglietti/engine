BEGIN TRANSACTION;

PRAGMA foreign_keys = on;

create table public_key_authorizations(
 id integer primary key,
 date_insert datetime default CURRENT_TIMESTAMP,
 
 public_key integer not null,
 key_limit integer not null,
 
 constraint unq_public_key_attribute unique (public_key)
);

create table public_key_invoices(
 id integer primary key,
 date_insert datetime default CURRENT_TIMESTAMP,
 
 public_key_ref integer not null,
 amount text not null,
 
 constraint fk_invoices_pt_authorizations foreign key (public_key_ref) references public_key_authorizations (id)
);

COMMIT;
